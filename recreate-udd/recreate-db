#!/bin/bash
set -x
set -e

# stop all processes accessing UDD by restarting postgresql
if [ -d /srv/udd/init ] ; then
    cd /srv/udd/init
fi

if [ "$1" == "" -o "$1" != "--no-get" ] ; then
  if [ -e udd.dump ] ; then
    mv udd.dump udd.dump~
  fi
  wget --continue https://udd.debian.org/dumps/udd.dump
fi

# This interrupts other connections like commitstat gatherer
#if [ -x /usr/local/bin/postgresql_restart ] ; then
#   /usr/local/bin/postgresql_restart || true
#else
#   sudo service postgresql restart
#fi
if ! dropdb udd 2>dropdb_error.log ; then
  # Sometimes there are open connections that prevent droping the DB
  # https://stackoverflow.com/questions/17449420/postgresql-unable-to-drop-database-because-of-some-auto-connections-to-db
  psql udd <<EOT
REVOKE CONNECT ON DATABASE udd FROM public;
SELECT pid, pg_terminate_backend(pid) 
FROM pg_stat_activity 
WHERE datname = current_database() AND pid <> pg_backend_pid();
EOT
  dropdb udd
fi
createdb -T template0 -E SQL_ASCII udd
echo 'CREATE EXTENSION debversion' | psql udd
pg_restore -j $(nproc) -v -d udd udd.dump 2>&1 | tee > restore-udd.log || true # some warnings should not stop the script

# Hmmm, sometimes a
#    psql udd -c 'ALTER TABLE carnivore_names ADD PRIMARY KEY (id, name) ;'
# is needed
psql udd -c 'CREATE EXTENSION tablefunc; GRANT EXECUTE ON FUNCTION crosstab(text,text) TO guest;' || true
./maintain_names_prefered.py
psql udd < create_names_prefered.sql
psql udd < closed_bugs.sql
