-- top 10 maintainers who has closed bugs as carnivore ID for more than one maintainer team
CREATE OR REPLACE FUNCTION bug_closer_ids_of_pkggroups(text[],int) RETURNS SETOF RECORD AS $$
  SELECT ce.id
         , COUNT(*)::int
    FROM (SELECT id, source, done_email FROM archived_bugs
     UNION
     SELECT id, source, done_email FROM bugs WHERE status = 'done'
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
   WHERE source IN (           -- source packages that are maintained by the team
       SELECT DISTINCT source FROM upload_history
        WHERE maintainer_email = ANY ($1)
          AND nmu = 'f'
      )
    AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
       SELECT DISTINCT ce.email FROM upload_history uh
         JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
        WHERE maintainer_email = ANY ($1)
          AND nmu = 'f'
   )
   GROUP BY ce.id
   ORDER BY count DESC
   LIMIT $2
$$ LANGUAGE SQL;

/*
SELECT * FROM bug_closer_ids_of_pkggroups('{"debian-med-packaging@lists.alioth.debian.org"}',50) AS (id int, count int);
SELECT * FROM bug_closer_ids_of_pkggroups('{"debian-astro-maintainers@lists.alioth.debian.org"}',50) AS (id int, count int);
SELECT * FROM bug_closer_ids_of_pkggroups('{"pkg-osm-maint@lists.alioth.debian.org","pkg-grass-devel@lists.alioth.debian.org"}',50) AS (id int, count int);
SELECT * FROM bug_closer_ids_of_pkggroups('{"pkg-scicomp-devel@lists.alioth.debian.org","debian-science-maintainers@lists.alioth.debian.org"}',50) AS (id int, count int);
*/

-- top 10 maintainers who has closed bugs as carnivore ID for set of packages like 'r-%'
CREATE OR REPLACE FUNCTION bug_closer_ids_of_pkg_set(text,int) RETURNS SETOF RECORD AS $$
  SELECT ce.id
         , COUNT(*)::int
    FROM (SELECT id, source, done_email FROM archived_bugs
     UNION
     SELECT id, source, done_email FROM bugs WHERE status = 'done'
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
   WHERE source IN (           -- source packages that are maintained by the team
       SELECT DISTINCT source FROM packages
        WHERE package like $1
      )
    AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
       SELECT DISTINCT ce.email FROM upload_history uh
         JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
         JOIN packages pa         ON pa.source = uh.source
        WHERE pa.package like $1
   )
   GROUP BY ce.id
   ORDER BY count DESC
   LIMIT $2
$$ LANGUAGE SQL;

/*
SELECT * FROM bug_closer_ids_of_pkg_set('r-%',50) AS (id int, count int);
*/


-- top 10 maintainers closing bugs in team packages
CREATE OR REPLACE FUNCTION bug_closer_per_year_of_pkggroups(text[],int) RETURNS SETOF RECORD AS $$
  SELECT cn.name, db.year, COUNT(*)::int FROM
--    (SELECT source, EXTRACT(year FROM done_date)::int AS year, done_email   -- done_date is always 1970-01-01
    (SELECT id, source, EXTRACT(year FROM last_modified)::int AS year, done_email FROM archived_bugs
     UNION
     SELECT id, source, EXTRACT(year FROM last_modified)::int AS year, done_email FROM bugs WHERE status = 'done'
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
    JOIN (SELECT * FROM carnivore_names
           WHERE id IN (SELECT idupl FROM bug_closer_ids_of_pkggroups($1, $2) AS (idupl int, count int))
    )  cn ON ce.id    = cn.id
    JOIN carnivore_names_prefered cnp ON cn.id = cnp.id
   WHERE source IN (    -- source packages that are maintained by the team
       SELECT DISTINCT source FROM upload_history
        WHERE maintainer_email = ANY ($1)
          AND nmu = 'f'
     )
     AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
      SELECT DISTINCT ce.email FROM upload_history uh
        JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
       WHERE maintainer_email = ANY ($1)
     )
     AND cn.name = cnp.name
   GROUP BY cn.name, db.year
   ORDER BY year, count DESC, cn.name
$$ LANGUAGE SQL;

/*
SELECT * FROM bug_closer_per_year_of_pkggroups('{"debian-med-packaging@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
SELECT * FROM bug_closer_per_year_of_pkggroups('{"pkg-osm-maint@lists.alioth.debian.org","pkg-grass-devel@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
SELECT * FROM bug_closer_per_year_of_pkggroups('{"pkg-scicomp-devel@lists.alioth.debian.org","debian-science-maintainers@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
*/

-- top 10 maintainers closing bugs in team packages
CREATE OR REPLACE FUNCTION bug_closer_per_year_of_pkg_set(text,int) RETURNS SETOF RECORD AS $$
  SELECT cn.name, db.year, COUNT(*)::int FROM
--    (SELECT source, EXTRACT(year FROM done_date)::int AS year, done_email   -- done_date is always 1970-01-01
    (SELECT id, source, EXTRACT(year FROM last_modified)::int AS year, done_email FROM archived_bugs
     UNION
     SELECT id, source, EXTRACT(year FROM last_modified)::int AS year, done_email FROM bugs WHERE status = 'done'
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
    JOIN (SELECT * FROM carnivore_names
           WHERE id IN (SELECT idupl FROM bug_closer_ids_of_pkg_set($1, $2) AS (idupl int, count int))
    )  cn ON ce.id    = cn.id
    JOIN carnivore_names_prefered cnp ON cn.id = cnp.id
   WHERE source IN (    -- source packages that are maintained by the team
       SELECT DISTINCT source FROM packages
        WHERE package like $1
     )
     AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
      SELECT DISTINCT ce.email FROM upload_history uh
        JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
        JOIN packages pa         ON pa.source = uh.source
        WHERE pa.package like $1
     )
     AND cn.name = cnp.name
   GROUP BY cn.name, db.year
   ORDER BY year, count DESC, cn.name
$$ LANGUAGE SQL;

/*
SELECT * FROM bug_closer_per_year_of_pkg_set('r-%',50) AS (name text, year int, count int);
*/

-- top 10 maintainers as (hopefully!!!) unique name for more than one team
CREATE OR REPLACE FUNCTION bug_closer_names_of_pkggroups(text[], int) RETURNS SETOF RECORD AS $$
  SELECT cnp.name FROM
    (SELECT id FROM bug_closer_ids_of_pkggroups($1, $2) AS (id int, count int)) au
    JOIN carnivore_names_prefered cnp ON au.id = cnp.id
$$ LANGUAGE SQL;

/*
SELECT * FROM bug_closer_names_of_pkggroups('{"debian-med-packaging@lists.alioth.debian.org"}',50) AS (name text);
SELECT * FROM bug_closer_names_of_pkggroups('{"pkg-osm-maint@lists.alioth.debian.org","pkg-grass-devel@lists.alioth.debian.org"}',50) AS (name text);
SELECT * FROM bug_closer_names_of_pkggroups('{"pkg-scicomp-devel@lists.alioth.debian.org","debian-science-maintainers@lists.alioth.debian.org"}',50) AS (name text);
*/

-- top 10 maintainers for package set
CREATE OR REPLACE FUNCTION bug_closer_names_of_pkg_set(text, int) RETURNS SETOF RECORD AS $$
  SELECT cnp.name FROM
    (SELECT id FROM bug_closer_ids_of_pkg_set($1, $2) AS (id int, count int)) au
    JOIN carnivore_names_prefered cnp ON au.id = cnp.id
$$ LANGUAGE SQL;

/*
SELECT * FROM bug_closer_names_of_pkg_set('r-%',50) AS (name text);
 */

/*****************************************************************
 * WNPP - newly created packages                                 *
 *****************************************************************/

CREATE OR REPLACE FUNCTION wnpp_select() RETURNS SETOF RECORD AS $$
  SELECT id, source, done_email, last_modified FROM
    ( SELECT DISTINCT id, source, done_email, last_modified, ROW_NUMBER() OVER (PARTITION BY source ORDER BY id) FROM
      (SELECT id, done_email, last_modified, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM bugs WHERE source = 'wnpp' AND status = 'done'
       UNION
       SELECT id, done_email, last_modified, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM archived_bugs WHERE source = 'wnpp'
      ) wnpp
    ) tmp WHERE ROW_NUMBER = 1
$$ LANGUAGE SQL;

/*
  SELECT * FROM wnpp_select() AS (id int, source text, done_email text, last_modified timestamp) where source = 'einsteinpy' ;
 */

-- top 10 maintainers who has closed wnpp bugs as carnivore ID for more than one maintainer team
CREATE OR REPLACE FUNCTION wnpp_closer_ids_of_pkggroups(text[],int) RETURNS SETOF RECORD AS $$
  SELECT ce.id
         , COUNT(*)::int
    FROM ( SELECT id, source, done_email FROM wnpp_select() AS (id int, source text, done_email text, last_modified timestamp) ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
   WHERE source IN (           -- source packages that are maintained by the team
       SELECT DISTINCT source FROM upload_history
        WHERE maintainer_email = ANY ($1)
          AND nmu = 'f'
      )
    AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
       SELECT DISTINCT ce.email FROM upload_history uh
         JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
        WHERE maintainer_email = ANY ($1)
          AND nmu = 'f'
   )
   GROUP BY ce.id
   ORDER BY count DESC
   LIMIT $2
$$ LANGUAGE SQL;

/*
SELECT * FROM wnpp_closer_ids_of_pkggroups('{"debian-med-packaging@lists.alioth.debian.org"}',50) AS (id int, count int);
SELECT * FROM wnpp_closer_ids_of_pkggroups('{"debian-astro-maintainers@lists.alioth.debian.org"}',50) AS (id int, count int);
SELECT * FROM wnpp_closer_ids_of_pkggroups('{"pkg-osm-maint@lists.alioth.debian.org","pkg-grass-devel@lists.alioth.debian.org"}',50) AS (id int, count int);
SELECT * FROM wnpp_closer_ids_of_pkggroups('{"pkg-scicomp-devel@lists.alioth.debian.org","debian-science-maintainers@lists.alioth.debian.org"}',50) AS (id int, count int);
*/

-- top 10 maintainers who has closed wnpp bugs as carnivore ID for set of packages like 'r-%'
CREATE OR REPLACE FUNCTION wnpp_closer_ids_of_pkg_set(text,int) RETURNS SETOF RECORD AS $$
  SELECT ce.id
         , COUNT(*)::int
    FROM ( SELECT id, source, done_email FROM wnpp_select() AS (id int, source text, done_email text, last_modified timestamp) ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
   WHERE source IN (           -- source packages that are maintained by the team
       SELECT DISTINCT source FROM packages
        WHERE package like $1
      )
    AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
       SELECT DISTINCT ce.email FROM upload_history uh
         JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
         JOIN packages pa         ON pa.source = uh.source
        WHERE pa.package like $1
   )
   GROUP BY ce.id
   ORDER BY count DESC
   LIMIT $2
$$ LANGUAGE SQL;

/*
SELECT * FROM wnpp_closer_ids_of_pkg_set('r-%',50) AS (id int, count int);
*/

-- top 10 maintainers closing wnpp bugs in team packages
CREATE OR REPLACE FUNCTION wnpp_closer_per_year_of_pkggroups(text[],int) RETURNS SETOF RECORD AS $$
  SELECT cn.name, db.year, COUNT(*)::int FROM
    (SELECT id, source, EXTRACT(year FROM last_modified)::int AS year, done_email FROM
      wnpp_select() AS (id int, source text, done_email text, last_modified timestamp)
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
    JOIN (SELECT * FROM carnivore_names
           WHERE id IN (SELECT idupl FROM wnpp_closer_ids_of_pkggroups($1, $2) AS (idupl int, count int))
    )  cn ON ce.id    = cn.id
    JOIN carnivore_names_prefered cnp ON cn.id = cnp.id
   WHERE source IN (    -- source packages that are maintained by the team
       SELECT DISTINCT source FROM upload_history
        WHERE maintainer_email = ANY ($1)
          AND nmu = 'f'
     )
     AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
      SELECT DISTINCT ce.email FROM upload_history uh
        JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
       WHERE maintainer_email = ANY ($1)
     )
     AND cn.name = cnp.name
   GROUP BY cn.name, db.year
   ORDER BY year, count DESC, cn.name
$$ LANGUAGE SQL;

/*
SELECT * FROM wnpp_closer_per_year_of_pkggroups('{"debian-med-packaging@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
SELECT * FROM wnpp_closer_per_year_of_pkggroups('{"debian-astro-maintainers@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
SELECT * FROM wnpp_closer_per_year_of_pkggroups('{"pkg-osm-maint@lists.alioth.debian.org","pkg-grass-devel@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
SELECT * FROM wnpp_closer_per_year_of_pkggroups('{"pkg-scicomp-devel@lists.alioth.debian.org","debian-science-maintainers@lists.alioth.debian.org"}',50) AS (name text, year int, count int);
*/

-- top 10 maintainers closing bugs in team packages
CREATE OR REPLACE FUNCTION wnpp_closer_per_year_of_pkg_set(text,int) RETURNS SETOF RECORD AS $$
  SELECT cn.name, db.year, COUNT(*)::int FROM
    (SELECT id, source, EXTRACT(year FROM last_modified)::int AS year, done_email FROM
      wnpp_select() AS (id int, source text, done_email text, last_modified timestamp)
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
    JOIN (SELECT * FROM carnivore_names
           WHERE id IN (SELECT idupl FROM wnpp_closer_ids_of_pkg_set($1, $2) AS (idupl int, count int))
    )  cn ON ce.id    = cn.id
    JOIN carnivore_names_prefered cnp ON cn.id = cnp.id
   WHERE source IN (    -- source packages that are maintained by the team
       SELECT DISTINCT source FROM packages
        WHERE package like $1
     )
     AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
      SELECT DISTINCT ce.email FROM upload_history uh
        JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
        JOIN packages pa         ON pa.source = uh.source
        WHERE pa.package like $1
     )
     AND cn.name = cnp.name
   GROUP BY cn.name, db.year
   ORDER BY year, count DESC, cn.name
$$ LANGUAGE SQL;

/*
SELECT * FROM wnpp_closer_per_year_of_pkg_set('r-%',50) AS (name text, year int, count int);
*/

-- top 10 maintainers as (hopefully!!!) unique name for more than one team
CREATE OR REPLACE FUNCTION wnpp_closer_names_of_pkggroups(text[], int) RETURNS SETOF RECORD AS $$
  SELECT cnp.name FROM
    (SELECT id FROM wnpp_closer_ids_of_pkggroups($1, $2) AS (id int, count int)) au
    JOIN carnivore_names_prefered cnp ON au.id = cnp.id
$$ LANGUAGE SQL;

/*
SELECT * FROM wnpp_closer_names_of_pkggroups('{"debian-med-packaging@lists.alioth.debian.org"}',50) AS (name text);
SELECT * FROM wnpp_closer_names_of_pkggroups('{"debian-astro-maintainers@lists.alioth.debian.org"}',50) AS (name text);
SELECT * FROM wnpp_closer_names_of_pkggroups('{"pkg-osm-maint@lists.alioth.debian.org","pkg-grass-devel@lists.alioth.debian.org"}',50) AS (name text);
SELECT * FROM wnpp_closer_names_of_pkggroups('{"pkg-scicomp-devel@lists.alioth.debian.org","debian-science-maintainers@lists.alioth.debian.org"}',50) AS (name text);
*/

-- top 10 maintainers for package set
CREATE OR REPLACE FUNCTION wnpp_closer_names_of_pkg_set(text, int) RETURNS SETOF RECORD AS $$
  SELECT cnp.name FROM
    (SELECT id FROM wnpp_closer_ids_of_pkg_set($1, $2) AS (id int, count int)) au
    JOIN carnivore_names_prefered cnp ON au.id = cnp.id
$$ LANGUAGE SQL;

/*
SELECT * FROM wnpp_closer_names_of_pkg_set('r-%',50) AS (name text);
 */


/*  Checks

-- r-Pakete tille@debian.org
SELECT id, done_email, EXTRACT(year FROM last_modified)::int AS year, source FROM
  ( SELECT id, done_email, last_modified, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM bugs WHERE source = 'wnpp' AND status = 'done'
    UNION
    SELECT id, done_email, last_modified, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM archived_bugs WHERE source = 'wnpp') tmp
   WHERE source like 'r-%' AND done_email = 'tille@debian.org' ORDER BY year, id;

-- r-Pakete tille@debian.org per year
 SELECT year, count(*) FROM (SELECT id, done_email, EXTRACT(year FROM last_modified)::int AS year, source FROM
  ( SELECT id, done_email, last_modified, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM bugs WHERE source = 'wnpp' AND status = 'done'
    UNION
    SELECT id, done_email, last_modified, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM archived_bugs WHERE source = 'wnpp') tmp
   WHERE source like 'r-%' AND done_email = 'tille@debian.org' ) tmp GROUP BY year ORDER BY year;

-- wnpp in debian-astro team
  SELECT (SELECT name FROM carnivore_names_prefered WHERE ce.id = id), COUNT(*)::int
    FROM (SELECT id, source, done_email FROM
      (SELECT id, done_email, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM bugs WHERE source = 'wnpp' AND status = 'done'
       UNION
       SELECT id, done_email, regexp_replace(title, '^[^:]+: ([^ ]+).*', '\1') AS source FROM archived_bugs WHERE source = 'wnpp'
      ) wnpp
    ) db
    JOIN carnivore_emails ce ON ce.email = db.done_email
   WHERE source IN (           -- source packages that are maintained by the team
       SELECT DISTINCT source FROM upload_history
        WHERE maintainer_email = 'debian-astro-maintainers@lists.alioth.debian.org'
          AND nmu = 'f'
      )
    AND done_email IN ( -- email of uploaders who at least once uploaded on behalf of the team
       SELECT DISTINCT ce.email FROM upload_history uh
         JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
        WHERE maintainer_email = 'debian-astro-maintainers@lists.alioth.debian.org'
          AND nmu = 'f'
   )
   GROUP BY ce.id
   ORDER BY count DESC
;



*/
