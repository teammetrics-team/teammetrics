#!/bin/sh -e

rsync -a -P \
  blends.debian.net:/var/backups/teammetrics/teammetrics.0.xz .
dropdb teammetrics || true
createdb teammetrics
xzcat teammetrics.0.xz | psql teammetrics
