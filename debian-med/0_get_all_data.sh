#!/bin/sh

./commits-R.sh
./commits.sh
./commits_installer.sh
./liststat_per_week.sh
./uploaders.sh

# Remove last line to avoid effect of incomplete weeks
sed -i -e '$d' *.dat
