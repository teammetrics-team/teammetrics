#!/bin/sh

DAT=uploaders.dat
DATU=uploads.dat
DATALL=uploads-all.dat
DATR=uploads-R.dat

# weekly data for uploads
psql udd --tuples-only > ${DATU} <<EOT
SELECT to_char(date_trunc('week', date), 'YYYY-MM-DD') AS week_start,
       count(*)
    FROM upload_history
    WHERE source IN (SELECT DISTINCT source FROM sources WHERE maintainer_email = 'debian-med-packaging@lists.alioth.debian.org')
GROUP BY week_start
ORDER BY week_start
;
EOT

# weekly data for uploads
psql udd --tuples-only > ${DAT} <<EOT
SELECT week_start, count(*) FROM (
   SELECT to_char(date_trunc('week', date), 'YYYY-MM-DD') AS week_start,
       changed_by_email,
       count(version)
    FROM upload_history
    WHERE source IN (SELECT DISTINCT source FROM sources WHERE maintainer_email = 'debian-med-packaging@lists.alioth.debian.org')
      AND changed_by_email != 'N/A'
     GROUP by week_start, changed_by_email
) tmp GROUP BY week_start
ORDER BY week_start
;
EOT

# weekly data for ALL uploads
psql udd --tuples-only > ${DATALL} <<EOT
SELECT to_char(date_trunc('week', date), 'YYYY-MM-DD') AS week_start,
       count(*)
    FROM upload_history
    WHERE source IN (SELECT DISTINCT source FROM sources)
GROUP BY week_start
ORDER BY week_start
;
EOT

# weekly data for uploads of pkg-r team
psql udd --tuples-only > ${DATR} <<EOT
SELECT to_char(date_trunc('week', date), 'YYYY-MM-DD') AS week_start,
       count(*)
    FROM upload_history
    WHERE source IN (SELECT DISTINCT source FROM sources WHERE maintainer_email = 'r-pkg-team@alioth-lists.debian.net')
GROUP BY week_start
ORDER BY week_start
;
EOT


sed -i 's/[[:space:]]\+|[[:space:]]\+/\t/' uploa*.dat

exit 0

# How many people contributed

# monthly data
DATM=commiters-month.dat
DAT=commiters.dat
DATnorm=commiters-normalised.dat

psql teammetrics --tuples-only > ${DATM} <<EOT
SELECT month, count(*) FROM (
   SELECT to_char(commit_date, 'YYYY') || '-' || to_char(commit_date, 'MM') AS month,
       name,
       count(commit_id)
   FROM commitstat
   WHERE project = 'debian-med'
   GROUP BY month, name
) tmp GROUP BY month
ORDER BY month
;
EOT

# weekly data
psql teammetrics --tuples-only > ${DAT} <<EOT
SELECT week_start, count(*) FROM (
   SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       name,
       count(commit_id)
   FROM commitstat
   WHERE project = 'debian-med'
   GROUP BY week_start, name
) tmp GROUP BY week_start
ORDER BY week_start
;
EOT


# weekly data but ignore commiters with less than 10 commits (no big difference in principle in the end, more than 3 commits does practically show nothing)
psql teammetrics --tuples-only > ${DATnorm} <<EOT
SELECT week_start, count(*) FROM (
   SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       name,
       count(commit_id)
   FROM commitstat
   WHERE project = 'debian-med'
     AND name IN (SELECT name FROM (SELECT name, COUNT(*) FROM commitstat WHERE project = 'debian-med' GROUP BY name) tmp WHERE count >= 10)
   GROUP BY week_start, name
) tmp GROUP BY week_start
ORDER BY week_start
;
EOT


sed -i 's/[[:space:]]\+|[[:space:]]\+/\t/' commit*.dat

exit 0

# R plot monthly
#   https://www.r-bloggers.com/plot-weekly-or-monthly-totals-in-r/
