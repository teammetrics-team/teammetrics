#!/bin/sh

DAT=commits-installer.dat
DATALL=commits-all.dat

# weekly data
psql teammetrics --tuples-only > ${DAT} <<EOT
SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       count(commit_id)
FROM commitstat
WHERE project in ('debian-cd', 'd-i') AND name NOT IN ('Debian Janitor')
  AND commit_date <= CURRENT_DATE
GROUP BY week_start
ORDER BY week_start
;
EOT

# weekly data
psql teammetrics --tuples-only > ${DATALL} <<EOT
SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       count(commit_id)
FROM commitstat
WHERE name NOT IN ('Debian Janitor')
  AND commit_date <= CURRENT_DATE
GROUP BY week_start
ORDER BY week_start
;
EOT


sed -i 's/[[:space:]]\+|[[:space:]]\+/\t/' ${DAT} ${DATALL}

exit 0

# R plot monthly
#   https://www.r-bloggers.com/plot-weekly-or-monthly-totals-in-r/
