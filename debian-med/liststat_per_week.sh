#!/bin/sh

DAT=liststat.dat
DATA=liststat-authors.dat

# Mails sent per week
psql teammetrics --tuples-only > ${DAT} <<EOT
SELECT to_char(date_trunc('week', archive_date), 'YYYY-MM-DD') AS week_start,
       count(message_id)
FROM listarchives
WHERE project = 'debian-med'
GROUP BY week_start
ORDER BY week_start
;
EOT

# Authors of mails
psql teammetrics --tuples-only > ${DATA} <<EOT
SELECT week_start, count(*) FROM (
   SELECT to_char(date_trunc('week', archive_date), 'YYYY-MM-DD') AS week_start,
       name,
       count(message_id)
   FROM listarchives
   WHERE project = 'debian-med' and name != 'Debian Janitor'
   GROUP BY week_start, name
) tmp GROUP BY week_start
ORDER BY week_start
;
EOT


sed -i 's/[[:space:]]\+|[[:space:]]\+/\t/' liststat*.dat

exit 0

# R plot monthly
#   https://www.r-bloggers.com/plot-weekly-or-monthly-totals-in-r/
