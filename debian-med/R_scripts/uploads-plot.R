suppressPackageStartupMessages({
  library(tidyverse) # apt install r-cran-tidyverse
  library(lubridate) # apt install r-cran-lubridate
  library(reshape)   # apt install r-cran-reshape
})

source("static-data.R")

# Load data from file
commiters <- read.table("../uploads.dat",
                        header = FALSE,
                        sep = "\t")

# V1 column is date column. Currently as factor so convert to date class
# in the format of YYYY-MM-DD
commiters$V1 <- ymd(commiters$V1)

if (uploads.xscale[2] == "auto")
	uploads.xscale <- c(uploads.xscale[1], tail(commiters$V1, n = 1))

# Form our graph object as commiters.graph
commiters.graph <- commiters %>%
  ggplot(aes(x = V1, y = V2)) +
  xlab("Date") +
  ylab("Uploads") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1),
        text = element_text(size = 10)) +
  scale_x_date(breaks = uploads.xbreaks,
               limits = ymd(uploads.xscale),
               expand = c(0, 0),
               date_labels = "%Y (%b)") +
  labs(caption = "Shaded region signifies last six months before Alioth to Salsa migration") +
  geom_line(group = 1)

# Alioth's last 6 months
commiters.graph <- commiters.graph +
  annotate("rect", xmin = ymd(alioth.begin_region),
           xmax = ymd(alioth.end_region), ymin = -Inf, ymax = Inf,
           alpha = .35)

# Construct all vertical lines
d <- rbind(data.frame(date = ymd(release.dates),         color = "Release" ),
           data.frame(date = ymd(sprint.start_dates),    color = "Sprint"  ),
           data.frame(date = ymd(hackathon.start_dates), color = "Hackathon"))

commiters.graph <- commiters.graph +
  geom_vline(aes(xintercept = date, color = color),
             d,
             linetype = "dotted") +
  scale_color_manual(name = "Events",
                     values = global.dottedvline.colours)

# Save as PDF
ggsave(
  filename = "uploads.pdf",
  plot = commiters.graph,
  device = "pdf",
  width = 192.0,
  height = 108.9,
  units = "mm"
)
print("***Exported to current directory.***")
