#!/bin/sh

DATM=commits-month.dat
DAT=commits.dat

# monthly data
psql teammetrics --tuples-only > ${DATM} <<EOT
SELECT to_char(commit_date, 'YYYY') || '-' || to_char(commit_date, 'MM') AS month,
       count(commit_id)
FROM commitstat
WHERE project = 'debian-med' AND name NOT IN ('Debian Janitor', 'Debian Med Packaging Team')
GROUP BY month
ORDER BY month
;
EOT

# weekly data
psql teammetrics --tuples-only > ${DAT} <<EOT
SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       count(commit_id)
FROM commitstat
WHERE project = 'debian-med' AND name NOT IN ('Debian Janitor', 'Debian Med Packaging Team')
  AND commit_date <= CURRENT_DATE
GROUP BY week_start
ORDER BY week_start
;
EOT

# How many people contributed

# monthly data
DATM=commiters-month.dat
DAT=commiters.dat
DATnorm=commiters-normalised.dat

psql teammetrics --tuples-only > ${DATM} <<EOT
SELECT month, count(*) FROM (
   SELECT to_char(commit_date, 'YYYY') || '-' || to_char(commit_date, 'MM') AS month,
       name,
       count(commit_id)
   FROM commitstat
   WHERE project = 'debian-med' AND name NOT IN ('Debian Janitor', 'Debian Med Packaging Team')
     AND commit_date <= CURRENT_DATE
   GROUP BY month, name
) tmp GROUP BY month
ORDER BY month
;
EOT

# weekly data
psql teammetrics --tuples-only > ${DAT} <<EOT
SELECT week_start, count(*) FROM (
   SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       name,
       count(commit_id)
   FROM commitstat
   WHERE project = 'debian-med' AND name NOT IN ('Debian Janitor', 'Debian Med Packaging Team')
     AND commit_date <= CURRENT_DATE
   GROUP BY week_start, name
) tmp GROUP BY week_start
ORDER BY week_start
;
EOT


# weekly data but ignore commiters with less than 10 commits (no big difference in principle in the end, more than 3 commits does practically show nothing)
psql teammetrics --tuples-only > ${DATnorm} <<EOT
SELECT week_start, count(*) FROM (
   SELECT to_char(date_trunc('week', commit_date), 'YYYY-MM-DD') AS week_start,
       name,
       count(commit_id)
   FROM commitstat
   WHERE project = 'debian-med'
     AND name IN (SELECT name FROM (SELECT name, COUNT(*) FROM commitstat WHERE project = 'debian-med' and name != 'Debian Janitor' GROUP BY name) tmp WHERE count >= 10)
     AND commit_date <= CURRENT_DATE
   GROUP BY week_start, name
) tmp GROUP BY week_start
ORDER BY week_start
;
EOT


sed -i 's/[[:space:]]\+|[[:space:]]\+/\t/' commit*.dat

exit 0

# R plot monthly
#   https://www.r-bloggers.com/plot-weekly-or-monthly-totals-in-r/
