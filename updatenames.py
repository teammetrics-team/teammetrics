#!/usr/bin/python3

"""Update the database with a single name for active posters that have been posting under multiple names.

The posters and their multiple names are stored in a mapping of real name to the variation:

        UPDATE listarchives SET name='Name' WHERE name ILIKE $FOO
"""

import os
import codecs
import logging

import psycopg2

import liststat

PROJECT_DIR = './etc/teammetrics/'
NAME_FILE = os.path.join(PROJECT_DIR, 'names.list')
EMAIL_FILE = os.path.join(PROJECT_DIR, 'emails.list')
BOT_FILE = os.path.join(PROJECT_DIR, 'bots.list')


def parse_names():
    """Read NAME_FILE and return a mapping of real-name: variations."""
    name_variation = {}
    with codecs.open(NAME_FILE, encoding='utf-8') as f:
        for line in f:
            name, variations = line.split(':')
            name_variation[name.strip()] = [element.strip() for element in variations.split(',')]

    return name_variation

def parse_emails():
    """Read EMAIL_FILE and return a mapping of real-name: e-mail mappings."""
    name_variation = {}
    with codecs.open(EMAIL_FILE, encoding='utf-8') as f:
        for line in f:
            name, variations = line.split(':')
            name_variation[name.strip()] = [element.strip() for element in variations.split(',')]

    return name_variation


def parse_bots():
    """Read BOT_FILE and return the list of robots."""
    bots = [line.strip() for line in open(BOT_FILE)]
    return bots


def update_names(conn, cur, table='listarchives'):
    """Update the names with the items in NAMES."""
    emails = parse_emails()
    emailstring = 'email_addr'
    if table == 'commitstat':
        emailstring = 'email'
    for key, item in emails.items():
        query = """UPDATE {0}
                SET name = '{1}'
                WHERE {2} = ANY (%s);""".format(table, key, emailstring)

        #print(query)
        #cur.mogrify(query, (item,))
        cur.execute(query, (item,))
        conn.commit()

    names = parse_names()
    for key, item in names.items():
        query = """UPDATE {0}
                SET name = %s
                WHERE name ILIKE %s""".format(table)

        if len(item) > 1:
            for i in range(len(item)-1):
                query += " OR name ILIKE %s"
            item.insert(0, key)
            query += ';'
            cur.execute(query, item)
            conn.commit()

        else:
            query += ';'
            cur.execute(query, (key, ''.join(item)))
            conn.commit()
    # Remove the bots from the list.
    bots = parse_bots()
    for name in bots:
        query = """DELETE FROM {0}
                WHERE name ILIKE %s;""".format(table)
        cur.execute(query, (name,))
        conn.commit()


if __name__ == '__main__':
    DATABASE = liststat.DATABASE
    # Connect to the database.
    try:
        conn = psycopg2.connect(database=DATABASE['name'], port=DATABASE['defaultport'])
    except psycopg2.OperationalError:
        conn = psycopg2.connect(database=DATABASE['name'], port=DATABASE['port'])
    cur = conn.cursor()

    # Update the 'listarchives' and 'commitstat' tables.
    update_names(conn, cur)
    update_names(conn, cur, 'commitstat')

    # It seems names spelled in arabic will not be catched by the query above
    # Fix Ahmed El-Mahmoudy manually here
    query = """UPDATE listarchives
               SET name ='Ahmed El-Mahmoudy'
               WHERE email_addr LIKE 'aelmahmoudy@%' OR message_id like '2%.G%@ants.dhis.net'
            """
    cur.execute(query)
    conn.commit()
    # Similar issue with this indian dialect
    query = """UPDATE listarchives
               SET name ='shirish शिरीष <'
               WHERE email_addr = 'shirishag75@gmail.com'
            """
    # No "like" implemented for e-mail addresses
    query = """UPDATE listarchives
               SET name = 'Joost van Baal-Ilić'
               WHERE email_addr like 'joostvb-debian-%@mdcc.cx'
            """
    query = """UPDATE listarchives
               SET name = 'Loïc Minier'
               WHERE email_addr like '%lool@dooz.org%'
            """
    query = """UPDATE listarchives
               SET name = 'Rogério Brito'
               WHERE email_addr like '%rbrito@ime.usp.br%'
            """
    cur.execute(query)
    conn.commit()
